#!/bin/bash
HELP_TEXT="Help:\n
\nthisScript host port user password binary_of_remote_server param1 param2 param3
\n
---------------\n
Usually the default ssh port is 22\n
----------------\n
(REQUIRED) host port user password binary_of_remote_server\n
----------------\n
(OPTIONAL) param1 param2 param3\n
----------------\n
Example of conection (Opening the remote Sublime):\n
thisScript 192.168.100.101 22 john pass123 /usr/bin/subl /home/remoteuser/doc.txt\n


"
if [ ! "$1" ]; then
echo -e $HELP_TEXT
exit;
fi

##Old Verification
#if [ $1 != '' ] && [ $2 != '' ] && [ $3 != '' ] && [ $4 != '' ] && [ $5 != '' ]; then

if [ "$1" ] && [ "$2" ] && [ "$3" ] && [ "$4" ] && [ "$5" ]; then
./__expect.sh "$@"
else
echo -e $HELP_TEXT
exit;
fi
