## Executa uma aplicação remota de um servidor ssh.
### Exemplo:
``` exec-expect.sh 127.0.0.1 22 user pass123 /usr/bin/subl ```



```
----------- CREDITS -------------
Original por Wang Yong <wangyong@deepin.com>
Adaptado para execução remota de aplicaçoes por:
Tiago França <tiagofranca.com>
#v0.1.0 2019-06-23
---------------------------------

```

## IMPORTANTE:
Sempre execute o 'exec-expect.sh',pois é ele quem recebe os parâmetros e passa para o '__expect.sh'.
Executar o '__expect.sh' direto não fará mal, só não funcionará ;)