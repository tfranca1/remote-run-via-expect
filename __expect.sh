#! /usr/bin/expect -f

# Copyright (C) 2011 ~ 2016 Deepin, Inc.
#               2011 ~ 2016 Wang Yong
#
# Author:     Wang Yong <wangyong@deepin.com>
# Maintainer: Wang Yong <wangyong@deepin.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

## All possible interactive messages:
# Are you sure you want to continue connecting (yes/no)?
# password:
# Enter passphrase for key

## Main
# Delete self for secret, will not affect the following code
#file delete $argv0

##Simple string var
#set user {tiago}

# Setup variables
# Set timeout -1 to avoid remote server dis-connect.
# 0     1   2       3       4                       5      6     7

#host
set file_arg0 [lindex $argv 0]
#port
set file_arg1 [lindex $argv 1]
#user
set file_arg2 [lindex $argv 2]
#password
set file_arg3 [lindex $argv 3]
#binary_of_remote_server
set file_arg4 [lindex $argv 4]
#param1
set bin_param1 [lindex $argv 5]
#param2
set bin_param2 [lindex $argv 6]
#param3
set bin_param3 [lindex $argv 7]

set timeout 4

set server $file_arg0
set port $file_arg1
set user $file_arg2
set password $file_arg3
set application_bin $file_arg4

set private_key {}
set authentication {no}
set credits {"
----------- CREDITS -------------
Original por Wang Yong <wangyong@deepin.com>\n
Adaptado para execução remota de aplicaçoes por:\n
Tiago França <tiagofranca.com>\n
#v0.1.0 2019-06-23\n
---------------------------------
"}

# if {$application_arg != ""} {
    # exit;
# }

##Testa se existe
#if {[info exists file_arg1z]} {
#    set file_arg1 {sssss}
#}

##Verifica se a variável foi informada por argumentos
#if {$application_arg != ""} {
#    set application_bin application_arg
#}



set ssh_cmd {/usr/lib/deepin-terminal/zssh -X -o ServerAliveInterval=60}
#set ssh_opt {$user@$server -p $port -o PubkeyAuthentication=$authentication}
set ssh_opt  {$user@$server -p $port -o PubkeyAuthentication=$authentication -X -C $application_bin $bin_param1 $bin_param2 $bin_param3}
set remote_command { && echo -e $credits && }

# This code is use for synchronous pty's size to avoid terminal not update if login in remote server.
trap {
    stty rows [stty rows] columns [stty columns] < $spawn_out(slave,name)
} WINCH

# Spawn and expect
eval spawn $ssh_cmd $ssh_opt $private_key -t $remote_command exec \\\$SHELL -l
if {[string length $password]} {
    expect {
        timeout {send_user "ssh connection time out, please operate manually\n"}
        -nocase "(yes/no)\\?" {send "yes\r"; exp_continue}
        -nocase -re "password:|enter passphrase for key" {
            send "$password\r"
		}
	}
	
}
interact

